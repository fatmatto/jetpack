declare module "@fatmatto/jetpack" {
  type Configuration = {
    logger: { type: 'console' | 'noop' }
  } | {
    serviceName: string
    logger: {
      type: 'file'
      directory: string
      maxsize?: number
      maxFiles?: number
    }
  }

  export type JetpackLogger = {
    log: (
      level: 'error' |'warn' |'info' |'http' |'verbose' |'debug' | 'silly',
      msg: any,
      context?: any
    ) => void
  }

  const Logger: {
    getLogger(config: Configuration): JetpackLogger
    logFinishedRequests: (req, res, next) => void
  }
  const Tracing: any
  const Queue: any

  export { Logger, Tracing, Queue }
}