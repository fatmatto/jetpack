'use strict'
const amqp = require('amqplib')
const { EventEmitter } = require('events')
const { v4: uuidv4 } = require('uuid')

class RabbitMQQueueClient extends EventEmitter {
  /**
   *
   * @param {Object} config
   * @param {String} config.uri RabbitMQ uri to connect to
   * @param {String} config.id Consumer/Producer id
   * @param {String} config.queueName RabbitMQ queue name
   */
  constructor (config = {}) {
    super()

    if (!config.uri) {
      throw new Error('Missing required parameter uri')
    }

    if (!config.queueName) {
      throw new Error('Missing required parameter queueName')
    }

    this.uri = config.uri
    this.id = config.id || uuidv4()
    this.queueName = config.queueName

    this.connection = null
    this.channel = null
    this.exchange = null
    this.queue = null
  }

  async initConnection () {
    this.connection = await amqp.connect(this.uri)
    this.channel = await this.connection.createChannel()
  }

  async initConsumer () {
    if (!this.channel) {
      await this.initConnection()
    }

    await this.initQueue()
    await this.channel.prefetch(0)
  }

  async initExchange () {
    if (!this.channel) {
      await this.initConnection()
    }

    this.exchange = await this.channel.assertExchange(this.queueName, 'fanout', { durable: true })
  }

  async initProducer () {
    if (!this.channel) {
      await this.initConnection()
    }

    await this.initExchange()
    await this.initQueue()
    await this.channel.bindQueue(this.queueName, this.queueName)
  }

  async initQueue () {
    if (!this.channel) {
      await this.initConnection()
    }

    this.queue = await this.channel.assertQueue(this.queueName, { durable: true })
  }

  async push (data, override = {}) {
    if (!this.channel) {
      await this.initProducer()
    }

    const messages = []
    if (Array.isArray(data)) {
      messages.push(...data)
    } else {
      messages.push(data)
    }

    const queueName = override.queueName || this.queueName
    const payload = JSON.stringify(messages)
    const isChannelBufferFree = this.channel.publish(queueName, 'defaultRoutingKey', Buffer.from(payload))
    if (!isChannelBufferFree) {
      throw new Error("Channel's write buffer is full, please try again later")
    }
  }

  async listenForMessages () {
    if (!this.channel) {
      await this.initConsumer()
    }

    this.recoverInterval = setInterval(async () => {
      await this.channel.recover()
    }, 500)

    await this.channel.consume(this.queueName, message => {
      if (!message) {
        return
      }

      try {
        const parsed = JSON.parse(message.content.toString())
        if (Array.isArray(parsed)) {
          message.messages = parsed
        } else {
          Object.assign(message, parsed)
        }

        this.emit('message', message)
      } catch (e) {
        this.emit('error', e)
      }
    }, { consumerTag: this.id })
  }

  async pause () {
    clearInterval(this.recoverInterval)
    await this.channel.cancel(this.id)
  }

  resume () {
    return this.listenForMessages()
  }

  ack (message) {
    try {
      this.channel.ack(message)
      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  nak (message) {
    try {
      this.channel.reject(message, true)
      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  term (message) {
    try {
      this.channel.reject(message, false)
      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }
}

module.exports = { RabbitMQQueueClient }
