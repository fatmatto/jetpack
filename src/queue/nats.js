'use strict'
const { EventEmitter } = require('events')
const nats = require('nats')
const { v4: uuidv4 } = require('uuid')

/**
 * Client class for reading and writing to a NATS Queue.
 * @extends EventEmitter
 */
class NATSQueueClient extends EventEmitter {
  /**
   *
   * @param {Object} config
   * @param {String} config.uri NATS server uri
   * @param {String} config.id Consumer/Producer id
   * @param {String} config.streamName Jetstream stream name
   * @param {String} config.queueName Jetstream subject
   */
  constructor (config = {}) {
    super()

    if (!config.uri) {
      throw new Error('Missing required parameter uri')
    }

    if (!config.queueName) {
      throw new Error('Missing required parameter queueName')
    }

    this.uri = config.uri
    this.id = config.id || uuidv4()
    this.streamName = config.streamName || this.id
    this.queueName = config.queueName
    this.queueGroup = config.queueGroup

    this.natsConnection = null
    this.jetstreamClient = null
    this.jetstreamManager = null
    this.consumer = null
    this.producer = null
    this.codec = nats.JSONCodec()
  }

  async initConnection () {
    this.natsConnection = await nats.connect({
      servers: this.uri,
      maxReconnectAttempts: -1
    })
  }

  async initConsumer () {
    if (!this.natsConnection) {
      await this.initConnection()
    }

    await this.initStream()
    this.jetstreamClient = this.natsConnection.jetstream()

    const opts = nats.consumerOpts()
    opts.manualAck()
    opts.deliverNew()
    opts.durable(this.id)
    opts.queue(this.queueGroup)
    opts.deliverTo(this.queueGroup)
    opts.callback((e, message) => {
      if (e) {
        this.emit('error', e)
        return
      }

      if (!message) {
        return
      }

      try {
        const parsed = this.codec.decode(message.data)
        if (Array.isArray(parsed)) {
          message.messages = parsed
        } else if ('data' in parsed) {
          message.messages = [parsed]
        } else {
          Object.assign(message, parsed)
        }

        message.working()
        this.emit('message', message)
      } catch (e) {
        this.emit('error', e)
      }
    })

    this.consumer = await this.jetstreamClient.subscribe(`${this.streamName}.${this.queueName}`, opts)
  }

  async initProducer () {
    if (!this.natsConnection) {
      await this.initConnection()
    }

    await this.initStream()
    this.producer = this.natsConnection.jetstream()
  }

  async initStream () {
    if (!this.natsConnection) {
      await this.initConnection()
    }

    this.jetstreamManager = await this.natsConnection.jetstreamManager()

    try {
      await this.jetstreamManager.streams.find(`${this.streamName}.*`)
    } catch (e) {
      if (e.message !== 'no stream matches subject') {
        throw e
      }

      await this.jetstreamManager.streams.add({
        name: this.streamName,
        subjects: [`${this.streamName}.*`],
        retention: 'limits',
        max_age: 24 * 60 * 60 * 1000 * 1000 * 1000 // 24h in nanoseconds
      })
    }
  }

  async push (data, override = {}) {
    if (!this.producer) {
      await this.initProducer()
    }

    const messages = []
    if (Array.isArray(data)) {
      messages.push(...data)
    } else {
      messages.push(data)
    }

    const streamName = override.streamName || this.streamName
    const queueName = override.queueName || this.queueName
    const responses = []
    for (const message of messages) {
      const response = await this.producer.publish(`${streamName}.${queueName}`, this.codec.encode(message))
      responses.push(response)
    }

    if (!Array.isArray(data)) {
      return responses[0]
    }

    return responses
  }

  /**
   * Starts polling thr queue for messages. When messages are found (even 0 messages), the `messages` event is emitted
   * When an error occurs it emits the `error` event.
   */
  async listenForMessages () {
    if (!this.consumer) {
      await this.initConsumer()
    }
  }

  pause () {
    try {
      this.consumer.unsubscribe()
      this.consumer = null
      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  resume () {
    return this.listenForMessages()
  }

  ack (message) {
    try {
      message.ack()
      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  nak (message) {
    try {
      message.nak(100)
      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  term (message) {
    try {
      message.term()
      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }
}

module.exports = { NATSQueueClient }
