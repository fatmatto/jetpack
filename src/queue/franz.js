'use strict'
const Errors = require('throwable-http-errors')
const { EventEmitter } = require('events')
const request = require('@fatmatto/ptth')
const { v4: uuidv4 } = require('uuid')
const { whilst } = require('async')

/**
 * Client class for reading and writing to a Franz Queue.
 * @extends EventEmitter
 */
class FranzQueueClient extends EventEmitter {
  /**
   *
   * @param {Object} config
   * @param {String} config.uri Queue uri
   * @param {String} config.id Consumer/Producer id
   * @param {String} config.queueName Queue name
   */
  constructor (config = {}) {
    super()

    if (!config.uri) {
      throw new Error('Missing required parameter uri')
    }

    if (!config.queueName) {
      throw new Error('Missing required parameter queueName')
    }

    this.uri = config.uri
    this.queueName = config.queueName
    this.id = config.id || uuidv4()
  }

  async push (payload, override = {}) {
    const queueName = override.queueName || this.queueName
    const response = await request({
      method: 'POST',
      url: `${this.uri}/v1/queues/${queueName}/messages`,
      body: payload,
      headers: {
        'x-producer-id': this.id
      }
    })

    if (response.statusCode >= 400) {
      throw Errors.createErrorFromStatusCode(response.statusCode, response.body.error.message)
    }

    return response.body.data
  }

  /**
   * Starts polling thr queue for messages. When messages are found (even 0 messages), the `messages` event is emitted
   * When an error occurs it emits the `error` event.
   */
  listenForMessages () {
    this.ableToProcess = true
    return whilst(
      cb => cb(null, this.ableToProcess),
      async () => {
        try {
          const response = await request({
            method: 'GET',
            url: `${this.uri}/v1/queues/${this.queueName}/messages`,
            query: { limit: 1 },
            headers: {
              'x-consumer-id': this.id
            }
          })

          if (response.statusCode >= 400) {
            throw Errors.createErrorFromStatusCode(response.statusCode, response.body.error.message)
          }

          if (response.body.data.length) {
            this.emit('message', response.body.data[0])
          }
        } catch (e) {
          this.emit('error', e)
        } finally {
          await new Promise(resolve => setTimeout(resolve, 250))
        }

        return this.ableToProcess
      }
    )
  }

  pause () {
    try {
      this.ableToProcess = false
      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  resume () {
    return this.listenForMessages()
  }

  ack (message) {
    return Promise.resolve()
  }

  nak (message) {
    return Promise.resolve()
  }

  term (message) {
    return Promise.resolve()
  }
}

module.exports = { FranzQueueClient }
