'use strict'
const { FranzQueueClient } = require('./franz')
const { KafkaQueueClient } = require('./kafka')
const { NATSQueueClient } = require('./nats')
const { RabbitMQQueueClient } = require('./rabbitmq')
const { MqttClient } = require('./mqtt')

/**
 *
 * @param {Object} config The global configuration
 * @param {String} config.queue.type Tells which kind of client to create. Value can be franz or kafka
 * @param {String} config.queue.uri Hostname of the queue (both kafka and franz)
 * @param {String} config.queue.id Client id of the queue client (in kafka's case its the group Id)
 * @param {Object} queueConfig queue config and overrides
 * @param {String} queueConfig.queueName Name of the queue / topic
 * @returns
 */
function buildQueueClient (config, queueConfig) {
  if (config.queue.type === 'franz') {
    const defaultFranzConfig = {
      uri: config.queue.uri,
      id: config.queue.id
    }

    const finalConfig = {
      ...defaultFranzConfig,
      ...queueConfig
    }

    return new FranzQueueClient(finalConfig)
  } else if (config.queue.type === 'kafka') {
    const defaultKafkaConfig = {
      uri: config.queue.uri,
      id: config.queue.id // groupId del consumer
    }

    const finalConfig = {
      ...defaultKafkaConfig,
      ...queueConfig
    }

    return new KafkaQueueClient(finalConfig)
  } else if (config.queue.type === 'nats') {
    const defaultNATSConfig = {
      uri: config.queue.uri,
      id: config.queue.id
    }

    const finalConfig = {
      ...defaultNATSConfig,
      ...queueConfig
    }

    return new NATSQueueClient(finalConfig)
  } else if (config.queue.type === 'rabbitmq') {
    const defaultRabbitMQConfig = {
      uri: config.queue.uri,
      id: config.queue.id
    }

    const finalConfig = {
      ...defaultRabbitMQConfig,
      ...queueConfig
    }

    return new RabbitMQQueueClient(finalConfig)
  } else if (config.queue.type === 'mqtt') {
    const defaultMqttConfig = {
      uri: config.queue.uri,
      queueName: config.queue.queueName,
      options: config.queue.options,
      topicOptions: config.queue.topicOptions,
      id: config.queue.id
    }

    const finalConfig = {
      ...defaultMqttConfig,
      ...queueConfig
    }

    return new MqttClient(finalConfig)
  } else {
    throw new Error(`Unknown queue type ${config.queue.type}`)
  }
}

module.exports = { buildQueueClient }
