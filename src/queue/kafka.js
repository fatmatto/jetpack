'use strict'
const { EventEmitter } = require('events')
const { Kafka } = require('kafkajs')
const { v4: uuidv4 } = require('uuid')

const { CompressionTypes, CompressionCodecs } = require('kafkajs')
const SnappyCodec = require('kafkajs-snappy')

CompressionCodecs[CompressionTypes.Snappy] = SnappyCodec

class KafkaQueueClient extends EventEmitter {
  constructor (config = {}) {
    super()

    if (!config.uri) {
      throw new Error('Missing required parameter uri')
    }

    if (!config.queueName) {
      throw new Error('Missing required parameter queueName')
    }

    this.uri = config.uri
    this.id = config.id
    this.queueName = config.queueName
    this.producerSendAcks = config.producerSendAcks ?? 1

    this.kafka = new Kafka({
      brokers: this.uri.split(',')
    })
  }

  async initConsumer () {
    this.consumer = this.kafka.consumer({ groupId: this.id })
    await this.consumer.connect()
    await this.consumer.subscribe({ topics: [this.queueName] })
    await this.consumer.run({
      autoCommit: false,
      eachMessage: async ({ topic, partition, message }) => {
        try {
          message.topic = topic
          message.partition = partition
          const parsed = JSON.parse(message.value.toString())
          if (Array.isArray(parsed)) {
            message.messages = parsed
          } else {
            Object.assign(message, parsed)
          }

          this.emit('message', message)
        } catch (e) {
          await this.consumer.commitOffsets([{ topic, partition, offset: message.offset }])
          this.emit('error', e)
        }
      }
    })
  }

  async initProducer () {
    this.producer = this.kafka.producer()
    await this.producer.connect()
  }

  async push (data, override = {}) {
    if (!this.producer) {
      await this.initProducer()
    }

    const messages = []
    if (Array.isArray(data)) {
      messages.push(...data)
    } else {
      messages.push(data)
    }

    const queueName = override.queueName || this.queueName
    const key = uuidv4()
    const topicMessages = [{
      topic: queueName,
      messages: messages.map(m => ({ key, value: JSON.stringify(m) }))
    }]

    return this.producer.sendBatch({ topicMessages, acks: this.producerSendAcks })
  }

  async listenForMessages () {
    if (!this.consumer) {
      await this.initConsumer()
    }
  }

  pause () {
    return this.consumer.pause([{ topic: this.queueName }])
  }

  resume () {
    return this.consumer.resume([{ topic: this.queueName }])
  }

  ack (message) {
    return this.consumer.commitOffsets([
      { topic: message.topic, partition: message.partition, offset: message.offset }
    ])
  }

  nak (message) {
    return this.consumer.commitOffsets([
      { topic: message.topic, partition: message.partition, offset: message.offset }
    ])
  }

  term (message) {
    return this.consumer.commitOffsets([
      { topic: message.topic, partition: message.partition, offset: message.offset }
    ])
  }
}

module.exports = { KafkaQueueClient }
