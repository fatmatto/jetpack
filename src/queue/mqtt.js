'use strict'
const { EventEmitter } = require('events')
const MQTT = require('async-mqtt')

class MqttClient extends EventEmitter {
  constructor (config = {}) {
    super()

    if (!config.uri) {
      throw new Error('Missing required parameter uri')
    }

    if (!config.queueName) {
      throw new Error('Missing required parameter queueName')
    }

    this.uri = config.uri
    this.id = config.id
    this.options = config.options
    this.queueName = config.queueName
    this.topicOptions = config.topicOptions
  }

  async init () {
    this.client = await MQTT.connectAsync(this.uri, {
      clientId: this.id,
      ...this.options
    })
  }

  async push (message, opt) {
    if (!this.client) {
      await this.init()
    }

    await this.client.publish(this.queueName, JSON.stringify(message), opt)
  }

  async listenForMessages () {
    if (!this.client) {
      await this.init()
    }

    await this.client.subscribe(this.queueName, this.topicOptions)

    this.client.on('message', (topic, payload) => {
      this.emit('message', JSON.parse(payload.toString()))
    })

    this.client.on('error', (err) => {
      this.emit('error', err)
    })
  }

  async pause () {
    await this.client.end()
    this.client = null
  }

  async resume () {
    await this.listenForMessages()
  }

  ack () {
    return Promise.resolve()
  }

  nak () {
    return Promise.resolve()
  }

  term () {
    return Promise.resolve()
  }
}

module.exports = { MqttClient }
