'use strict'
const logger = require('./logger')
module.exports = {
  getLogger: logger.getLogger,
  logFinishedRequests: logger.logFinishedRequests
}
