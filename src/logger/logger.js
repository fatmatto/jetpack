'use strict'
const onFinished = require('on-finished')
const os = require('os')
const path = require('path')
const qs = require('querystring')
const winston = require('winston')

class ConsoleLogger {
  log (level, message, context) {
    console.log(JSON.stringify({ level, message, context }, null, 2))
  }
}

class FileLogger {
  constructor (config) {
    if (!config.serviceName) {
      throw new Error("Missing required configuration value 'serviceName'")
    }
    const transport = new winston.transports.File({
      filename: path.join(config.logger.directory, `${config.serviceName}.log`),
      zippedArchive: false,
      maxsize: config.logger.maxsize || 1024 * 1024 * 10,
      maxFiles: config.logger.maxFiles || 5
    })

    const format = winston.format.combine(
      winston.format.timestamp(),
      winston.format(info => {
        info.service = config.serviceName
        info.host = os.hostname()
        return info
      })(),
      winston.format.json()
    )

    this.logger = winston.createLogger({
      level: 'info',
      format,
      transports: [
        transport
      ]
    })
  }

  log (level, message, context) {
    return this.logger.log(level, message, context)
  }
}

class NoopLogger {
  log () {}
}

let instance
/**
 *
 * @param {Object} config
 * @param {Object} config.serviceName The name of the service
 * @param {Object} config.logger
 * @param {String} config.logger.type console | file | noop
 * @param {String} [config.logger.directory] Where to put file logs
 */
function getLogger (config) {
  if (!instance) {
    if (config.logger.type === 'console') {
      instance = new ConsoleLogger(config)
    } else if (config.logger.type === 'file') {
      instance = new FileLogger(config)
    } else if (config.logger.type === 'noop') {
      instance = new NoopLogger(config)
    } else {
      throw new Error(`Unkown logger type=${config.logger.type}`)
    }
  }

  return instance
}

function defaultRequestLoggerFormatter (req, res) {
  return {
    requestId: req?.id,
    statusCode: res?.statusCode,
    method: req?.method,
    url: qs.unescape(req?.originalUrl),
    path: qs.unescape(req?.path),
    traceId: req?.traceId,
    authentication: {
      userId: req?.authentication?.uuid
    }
  }
}

/**
 * Build a request logger middleware
 * Usage: app.use(buildRequestLogger({formatter: myFunction}))
 * Do not pass formatter to use the default one
 * @param {Object} config
 * @param {Function} config.formatter (req,res) => Object
 * @returns
 */
function buildRequestLogger (config = {}) {
  return (req, res, next) => {
    let log = null
    if (config.formatter && typeof config.formatter === 'function') {
      log = config.formatter(req, res)
    } else {
      log = defaultRequestLoggerFormatter(req, res)
    }

    onFinished(res, (err, res) => {
      const logger = getLogger()
      if (err) {
        logger.log('error', err.message, Object.assign(log, err))
      } else {
        logger.log('info', `${req.method} ${qs.unescape(req.originalUrl)} ${res.statusCode}`, log)
      }
    })

    next()
  }
}

function logFinishedRequests (req, res, next) {
  onFinished(res, (err, res) => {
    const logger = getLogger()
    const log = defaultRequestLoggerFormatter(req, res)
    if (err) {
      logger.log('error', err.message, Object.assign(log, err))
    } else {
      logger.log('info', `${req.method} ${qs.unescape(req.originalUrl)} ${res.statusCode}`, log)
    }
  })

  next()
}

module.exports = {
  ConsoleLogger,
  FileLogger,
  NoopLogger,
  getLogger,
  logFinishedRequests,
  buildRequestLogger
}
