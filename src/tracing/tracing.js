'use strict'
const { ResourceAttributes } = require('@opentelemetry/semantic-conventions')
const { Resource } = require('@opentelemetry/resources')
const { NodeTracerProvider } = require('@opentelemetry/node')
const { BatchSpanProcessor } = require('@opentelemetry/tracing')
const { registerInstrumentations } = require('@opentelemetry/instrumentation')
const { MongoDBInstrumentation } = require('@opentelemetry/instrumentation-mongodb')
const { ExpressInstrumentation } = require('@opentelemetry/instrumentation-express')
const { HttpInstrumentation } = require('@opentelemetry/instrumentation-http')
const { context, trace } = require('@opentelemetry/api')
const { JaegerExporter } = require('@opentelemetry/exporter-jaeger')
const { MongooseInstrumentation } = require('opentelemetry-instrumentation-mongoose')
const { getNodeAutoInstrumentations } = require('@opentelemetry/auto-instrumentations-node')

const Tracing = {}

/**
 * Initialize a new tracer instance
 * @param {Object} config
 * @param {String} config.serviceName Name of the current service
 * @param {String} config.tracing.host Host name of the tracing endpoint (Tempo/Jaeger)
 * @param {String} config.tracing.port Port number of the tracing endpoint (Tempo/Jaeger)
 */
function init (config) {
  const provider = new NodeTracerProvider({
    resource: new Resource({
      [ResourceAttributes.SERVICE_NAME]: config.serviceName
    })
  })
  registerInstrumentations({
    tracerProvider: provider,
    instrumentations: [
      new ExpressInstrumentation(),
      new MongoDBInstrumentation(),
      new HttpInstrumentation(),
      new MongooseInstrumentation(),
      getNodeAutoInstrumentations()
    ]
  })
  // Initialize the exporter.
  const options = {
    host: config.tracing.host,
    port: config.tracing.port,
    tags: [
      { key: 'service.name', value: config.serviceName }
    ]
  }
  const exporter = new JaegerExporter(options)
  /**
 *
 * Configure the span processor to send spans to the exporter
 * The SimpleSpanProcessor does no batching and exports spans
 * immediately when they end. For most production use cases,
 * OpenTelemetry recommends use of the BatchSpanProcessor.
 */
  provider.addSpanProcessor(new BatchSpanProcessor(exporter))

  provider.register()

  const tracer = trace.getTracer(config.serviceName)
  Tracing.tracer = tracer
  return tracer
}

function addTraceId (req, res, next) {
  const spanContext = trace.getSpanContext(context.active())
  req.traceId = spanContext && spanContext.traceId
  if (typeof res?.set === 'function') {
    res.set('X-Request-Id', req.traceId)
  }
  next()
}

function getTraceId () {
  const spanContext = trace.getSpanContext(context.active())
  return spanContext?.traceId
}

function getTracer () {
  return Tracing.tracer
}

function getContext () { return context }
function getTrace () { return trace }

module.exports = { Tracing, init, addTraceId, getTraceId, getTracer, getContext, getTrace }
