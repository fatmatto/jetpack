'use strict'
const T = require('./tracing')
module.exports = {
  init: T.init,
  addTraceId: T.addTraceId,
  getTraceId: T.getTraceId,
  getTracer: T.getTracer,
  getTrace: T.getTrace,
  getContext: T.getContext
}
