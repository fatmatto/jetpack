# 📦 1.2.1 (6 Oct 2022)
- [b0031](https://gitlab.com/fatmatto/jetpack/commit/b003168a86122c9f08d4c244c3366228ae62546d)  📦 Release 1.2.10 [skip ci]
- [64861](https://gitlab.com/fatmatto/jetpack/commit/64861707b15bd230516c30b1a6f7458b101d9c48)  Merge branch 'main' into 'main'
- [adc4a](https://gitlab.com/fatmatto/jetpack/commit/adc4ac90283cc189136b1394706ad2b4dab1903d)  fix: parsed message may be an array
- [6fd77](https://gitlab.com/fatmatto/jetpack/commit/6fd77fc2ffaac68018f025c4384a0c69b7cf730f)  chore: update dependencies
# 📦 1.2.9 (28 Sep 2022)
- [0e317](https://gitlab.com/fatmatto/jetpack/commit/0e317b8672250180227331db4e9b6ebe5f7d90a9)  📦 Release 1.2.9 [skip ci]
- [8783b](https://gitlab.com/fatmatto/jetpack/commit/8783b31c96316d3c324dc5d3267dd3e9bbd1a326)  Merge branch 'main' into 'main'
- [62d35](https://gitlab.com/fatmatto/jetpack/commit/62d35336a6180e7161fc092d5ccbba905598c48b)  fix: already in async conxtest, it's needed to use Promise.resolve()/Promise.reject()
- [39d14](https://gitlab.com/fatmatto/jetpack/commit/39d14973a43975ec67614260430536981a816dd6)  chore: parenthesis not needed
- [a1ffd](https://gitlab.com/fatmatto/jetpack/commit/a1ffd86d29b4ed9f18d8a21928b5e9cb6e89717e)  fix: this.channel.publish returns a boolean
- [f1871](https://gitlab.com/fatmatto/jetpack/commit/f18713b6c8cb95d0a3765b747d06cfc98af53e1c)  fix: function input should not be manipulated
- [414b7](https://gitlab.com/fatmatto/jetpack/commit/414b7e7146eb5e15827c2dc3d0f3e7de111d7de9)  refactor: using new Promise() in case of callbacks and Promise.resolve()/Promise.reject() in synchronous code
- [e73ba](https://gitlab.com/fatmatto/jetpack/commit/e73ba75afe81aaceed107f754099a74aaf3aa925)  fix: canceling error callback after connection and vice versa
- [bf199](https://gitlab.com/fatmatto/jetpack/commit/bf199d84e3547b77f836520f634ac27b92e4f7a6)  fix: better callback names
- [354b6](https://gitlab.com/fatmatto/jetpack/commit/354b6b13c73bbdcabda26021aa77af8c0b1ceb4b)  fix: callback for offsetOutOfRange returns the error, instead callbacks for rebalanced and rebalancing return nothing
- [bfea5](https://gitlab.com/fatmatto/jetpack/commit/bfea5d7f791a0cfcfbc22eaf3a8da8acd53522e6)  fix: use the word "config" instead of "conf" in all plugins
- [d8b3b](https://gitlab.com/fatmatto/jetpack/commit/d8b3b62cd585a734b0f7b11c85a6135e82e1864e)  fix: add event error on consumer
- [2fdc7](https://gitlab.com/fatmatto/jetpack/commit/2fdc7325eda4ef4c8e7f8eea936f515eadb31f9e)  fix: using method once instead of method on for events wrapped by Promise
- [f2c6f](https://gitlab.com/fatmatto/jetpack/commit/f2c6f3de3da024eeb2dd3d4092662c058e4183f1)  feat: let all methods return a Promise
- [e5ca5](https://gitlab.com/fatmatto/jetpack/commit/e5ca559fb06a6d82cfc49ded9d82b4d0a39a1b8a)  methods pause, resume, ack, nak and term must return undefined
- [7b511](https://gitlab.com/fatmatto/jetpack/commit/7b5115521009e3d9cab04c2417d8646cf43c7935)  chore: removing unnecessary "async" keywords
# 📦 1.2.8 (20 Sep 2022)
- [5f8f4](https://gitlab.com/fatmatto/jetpack/commit/5f8f4eae53f9f75802b361c0180359091a5547cb)  📦 Release 1.2.8 [skip ci]
- [efe27](https://gitlab.com/fatmatto/jetpack/commit/efe27413652188f4440ed02fd92bfd7515a331b1)  chore: add gitlab-ci config
- [1a487](https://gitlab.com/fatmatto/jetpack/commit/1a48760d4d08fb6a3d4e82ce5703c36e655a6546)  Merge branch 'main' into 'main'
- [7fca7](https://gitlab.com/fatmatto/jetpack/commit/7fca70a7d7745fa4c8412279de533d142c9c2d84)  fix: better use greater than or equal to prevent borderline cases
- [35496](https://gitlab.com/fatmatto/jetpack/commit/354962ec042e6b2c39b520b77c3bea57581ba1d2)  feat: introducing method pull for rabbitmq
# 📦 1.2.7 (20 Sep 2022)
- [a5ad0](https://gitlab.com/fatmatto/jetpack/commit/a5ad010d6f587b84ac2c1f8f85a35ebe85a1e249)  📦 Release 1.2.7 [skip ci]
- [8c820](https://gitlab.com/fatmatto/jetpack/commit/8c8203261c29b9b983bee009d0addd6ba4fc6059)  Merge branch 'main' into 'main'
- [4d37c](https://gitlab.com/fatmatto/jetpack/commit/4d37c99c32437738fda994d65817520b3466060d)  refactor: using events only in method listenForMessages, methods push and pull must return the exception in order the user can manage it
# 📦 1.2.6 (19 Sep 2022)
- [4cdae](https://gitlab.com/fatmatto/jetpack/commit/4cdae786b48e1086475bfc509acf8346894bb55b)  📦 Release 1.2.6 [skip ci]
- [c8bfb](https://gitlab.com/fatmatto/jetpack/commit/c8bfb4ebf748e37d4d8f7aca73ede02816c7a07e)  Merge branch 'main' into 'main'
- [a5d8c](https://gitlab.com/fatmatto/jetpack/commit/a5d8c23f6fc38cbdbb5f90c21c8fba8822d724c2)  fix: better to check for channel ready
- [af938](https://gitlab.com/fatmatto/jetpack/commit/af938621c5a93aeaa0d85cff33e3dd43631dc66a)  refactor: dropping unused method
# 📦 1.2.5 (16 Sep 2022)
- [b987b](https://gitlab.com/fatmatto/jetpack/commit/b987b66bb333d9881537af29d0d440942f2be331)  📦 Release 1.2.5 [skip ci]
- [52625](https://gitlab.com/fatmatto/jetpack/commit/526252542eec29ba65ba5ac991aa50f965c9034f)  Merge branch 'main' into 'main'
- [9ac39](https://gitlab.com/fatmatto/jetpack/commit/9ac39bf30d0e8c0c04456fbeebd36f48a54244a0)  refactor: deprecating option "autoACK"
- [4579b](https://gitlab.com/fatmatto/jetpack/commit/4579be5b3c88203301af14b391c8c2d6dec31e42)  refactor: avoid use of this.config: assigning properties to class
- [e7c69](https://gitlab.com/fatmatto/jetpack/commit/e7c697c9417b78690f100fed1add50ffb0ab4882)  chore: code style improvements
# 📦 1.2.4 (16 Sep 2022)
- [330aa](https://gitlab.com/fatmatto/jetpack/commit/330aa0ffcb9484070fc76b661b493a8ab2b5532b)  📦 Release 1.2.4 [skip ci]
- [b694a](https://gitlab.com/fatmatto/jetpack/commit/b694a2dbb2f74cb287fa1295cd3329d9e8d91698)  Merge branch 'main' into 'main'
- [3dab9](https://gitlab.com/fatmatto/jetpack/commit/3dab96be89880969f4962e500522f0aa83b91b44)  feat: auto creation of exchange and queue
- [72e8e](https://gitlab.com/fatmatto/jetpack/commit/72e8e42729b0c5caf1ec139cca97e73b2f436370)  chore: update dependencies
# 📦 1.2.3 (16 Sep 2022)
- [aec87](https://gitlab.com/fatmatto/jetpack/commit/aec8745da473ebaa03900fb1e72560ae9946dfc0)  📦 Release 1.2.3 [skip ci]
- [4786d](https://gitlab.com/fatmatto/jetpack/commit/4786dc595c1bb94a49027d5ee6f2cb2ab6fb959d)  Merge branch 'main' into 'main'
- [b0cfc](https://gitlab.com/fatmatto/jetpack/commit/b0cfca44f4e3d6c5fdd77556c55eb78a8a04c762)  fix: connection typos
# 📦 1.2.2 (9 Sep 2022)
- [5f198](https://gitlab.com/fatmatto/jetpack/commit/5f198da7696784f35400d662bfe9716fd66e98d4)  📦 Release 1.2.2 [skip ci]
- [8e4e0](https://gitlab.com/fatmatto/jetpack/commit/8e4e0415d4acb834a7997e49b260ef147f9fbd79)  Merge branch 'main' into 'main'
- [211f5](https://gitlab.com/fatmatto/jetpack/commit/211f5e5eeee882d76f58d304405f6d31003000cc)  add methods "ack", "nak" and "term" for all interfaces
# 📦 1.2.1 (14 Jun 2022)
- [948bf](https://gitlab.com/fatmatto/jetpack/commit/948bfdde0e53862ce3735eb0789a564ad67911f4)  📦 Release 1.2.1 [skip ci]
- [38020](https://gitlab.com/fatmatto/jetpack/commit/380204319b7daceb88d55e431753badcb1887f46)  Merge branch 'main' into 'main'
- [64b26](https://gitlab.com/fatmatto/jetpack/commit/64b26d97badcf86df542f4f10683335104275a8b)  fix: in case of errors, messages is NOT an array
# 📦 1.2.0 (14 Jun 2022)
- [d1c73](https://gitlab.com/fatmatto/jetpack/commit/d1c73354b991f3e3b9527c518aece3a33940c596)  📦 Release 1.2.0 [skip ci]
- [14b08](https://gitlab.com/fatmatto/jetpack/commit/14b0817f621c8cd3a47eaae136ee1e156b64ee48)  Merge branch 'main' into 'main'
- [c0180](https://gitlab.com/fatmatto/jetpack/commit/c018012856d385d1aaa408b62acd3c5b20c58a3b)  refactor(nats): using fetch api instead of pull
- [3411f](https://gitlab.com/fatmatto/jetpack/commit/3411fc42e026d7fc8d982646ff86207d5a91fdfc)  chore: update dependencies
- [cd7c5](https://gitlab.com/fatmatto/jetpack/commit/cd7c5dfd21a85e0a9d990736406203b93eb7af7e)  Merge branch 'main' into 'main'
- [b724e](https://gitlab.com/fatmatto/jetpack/commit/b724e2d5df059515ae864b0d1f6135855c76a814)  fix: force messages of stream to have a max age in order to preserve disk space
# 📦 1.1.9 (13 Apr 2022)
- [96cce](https://gitlab.com/fatmatto/jetpack/commit/96cce4c2f2000decc6788acae3c13ed559565a0d)  📦 Release 1.1.9 [skip ci]
- [be549](https://gitlab.com/fatmatto/jetpack/commit/be5498f391bbd1858e95c98ef08290157359e7f5)  chore: update dependencies
# 📦 1.1.8 (12 Apr 2022)
- [003fa](https://gitlab.com/fatmatto/jetpack/commit/003fa18b18a70900c5e4f28ef1a91ebb5e2dd866)  📦 Release 1.1.8 [skip ci]
- [8f19f](https://gitlab.com/fatmatto/jetpack/commit/8f19f544ad2f8b7c205fcb3ea4cf5362951107ea)  Merge branch 'main' into 'main'
- [fcbcb](https://gitlab.com/fatmatto/jetpack/commit/fcbcbbd723bdad99db48caf3f8d9c12a974cbbc4)  fix: sending every single message autonomously
- [4e289](https://gitlab.com/fatmatto/jetpack/commit/4e289d13f831ab7d2d49c1828db7fd2e8c57902d)  refactor: direct emitting of messages not pushing in array
# 📦 1.1.7 (8 Apr 2022)
- [f61bd](https://gitlab.com/fatmatto/jetpack/commit/f61bd500ebb68c763f0502ad8dc53d59b24ff4bd)  📦 Release 1.1.7 [skip ci]
- [76f29](https://gitlab.com/fatmatto/jetpack/commit/76f294485d008beec189d629e0b0c8d7c02379a7)  Merge branch 'main' into 'main'
- [212b2](https://gitlab.com/fatmatto/jetpack/commit/212b21802834f833ede09ff8665ceab203bb16c8)  fix: some properties in message are not enumerable, hence spread operator excludes them
# 📦 1.1.6 (1 Apr 2022)
- [f0c41](https://gitlab.com/fatmatto/jetpack/commit/f0c41ee6f6bc83fcd9462d660db4e7ef7c71f307)  📦 Release 1.1.6 [skip ci]
- [ddda3](https://gitlab.com/fatmatto/jetpack/commit/ddda33e8758400f3ee2f87d7b2768968be4a0e0a)  Merge branch 'main' into 'main'
- [7ab57](https://gitlab.com/fatmatto/jetpack/commit/7ab5709a5fb5d16d5b333a571c33b47366d9ea6b)  fix: using "id" for defining "groupId" in kafka and passing id in rabbitmq too
# 📦 1.1.5 (29 Mar 2022)
- [efedc](https://gitlab.com/fatmatto/jetpack/commit/efedcdb7da232692836b737c3cd2d3089909150b)  📦 Release 1.1.5 [skip ci]
- [bd8f3](https://gitlab.com/fatmatto/jetpack/commit/bd8f3b27d78d22b80863927d3020e585845afbea)  Merge branch 'main' into 'main'
- [006bc](https://gitlab.com/fatmatto/jetpack/commit/006bca3b456d3cc4f6708b56aefecc47d3ba4655)  fix: missing await
# 📦 1.1.4 (29 Mar 2022)
- [0dc27](https://gitlab.com/fatmatto/jetpack/commit/0dc27fcd23576eb904a328484e1abc55d2c017a1)  📦 Release 1.1.4 [skip ci]
- [3bb3c](https://gitlab.com/fatmatto/jetpack/commit/3bb3c14f955780687d76e5876076f63b50abab62)  Merge branch 'main' into 'main'
- [077ec](https://gitlab.com/fatmatto/jetpack/commit/077ec058ed986c296a32f6c7aff7972662524894)  fix: ensume stream exists also in case of consumer
# 📦 1.1.3 (24 Mar 2022)
- [097d5](https://gitlab.com/fatmatto/jetpack/commit/097d5f88c6b5454af71fe28d50598466499fad9e)  📦 Release 1.1.3 [skip ci]
- [8a0cd](https://gitlab.com/fatmatto/jetpack/commit/8a0cd76bfd7f1f2256b0ad743338dc54ccb3f363)  Merge branch 'main' of gitlab.com:fatmatto/jetpack
# 📦 1.1.2 (24 Mar 2022)
- [c9a65](https://gitlab.com/fatmatto/jetpack/commit/c9a6531d1a39a3c6b0b0e4788c7766316c659c12)  📦 Release 1.1.2 [skip ci]
- [8f9f9](https://gitlab.com/fatmatto/jetpack/commit/8f9f9bc3ff862d19d39a4d80267d4cb8722cb36a)  Merge branch 'main' into 'main'
- [2a858](https://gitlab.com/fatmatto/jetpack/commit/2a8582fe8b4910a1248ba25b87dfab6c27e2af24)  fix: better way for autoACK fallback in rabbitmq plugin
- [32087](https://gitlab.com/fatmatto/jetpack/commit/32087a188c20f5463934dcb1d9c6ccf052f475a2)  fix: jsdoc header update
- [b48c0](https://gitlab.com/fatmatto/jetpack/commit/b48c0aad20bde2b71f74a37040129a58f9143234)  fix: changed config parameters name for retro-compatibility with franz
- [1a022](https://gitlab.com/fatmatto/jetpack/commit/1a02256dbec6f0f6b96f6e9ea628ef23c1549bfc)  fix: always send an array, as in the others backends
- [cd447](https://gitlab.com/fatmatto/jetpack/commit/cd447aa7507b166cfb28c00650d9981ee19a867b)  fix: rolling back to single subject
- [79bb9](https://gitlab.com/fatmatto/jetpack/commit/79bb9ac2b2306687827384cc5570e79a7bb9667f)  fix: typos
- [c7b7d](https://gitlab.com/fatmatto/jetpack/commit/c7b7da0502a3f4a35b1161244070d0aa707b8a35)  fix: add missing export
- [92934](https://gitlab.com/fatmatto/jetpack/commit/9293456e0311ddbfedd2f7a78a64ac263ec0ac7f)  fix: merging properties
- [a3871](https://gitlab.com/fatmatto/jetpack/commit/a3871230e29a185155cc40440bb44565e39269ba)  fix: different subjects for producer and consumer
# 📦 1.1.1 (17 Mar 2022)
- [c6c58](https://gitlab.com/fatmatto/jetpack/commit/c6c581d587b711df02205b3231f33461aec7be41)  📦 Release 1.1.1 [skip ci]
- [b89db](https://gitlab.com/fatmatto/jetpack/commit/b89dbfb4cf0cf738ee0c04831560bd53073787af)  Merge branch 'main' into 'main'
- [c4c81](https://gitlab.com/fatmatto/jetpack/commit/c4c81e48a841cfd717028891a00def6638ac398c)  fix: avoid instance connection multiple times
# 📦 1.1.0 (17 Mar 2022)
- [ae7a4](https://gitlab.com/fatmatto/jetpack/commit/ae7a401ab0f1f21a8dfe6dde106eed276a34574e)  📦 Release 1.1.0 [skip ci]
- [df44d](https://gitlab.com/fatmatto/jetpack/commit/df44d1f6e0f146fc97dcec405713ee11f5ce8eaf)  Merge branch 'main' into 'main'
- [24bc9](https://gitlab.com/fatmatto/jetpack/commit/24bc90c32f1b429d231355cd30a3d270ff1fe8a6)  feat: add nats plugin
- [cf79a](https://gitlab.com/fatmatto/jetpack/commit/cf79a8d008c639822f0063ac5690c2ca70ba7e96)  chore: update dependencies
- [f020a](https://gitlab.com/fatmatto/jetpack/commit/f020acb46b7c8971be6178718e6f5ab989789675)  chore: removed unused dependency
- [026a7](https://gitlab.com/fatmatto/jetpack/commit/026a70c3032af68dd42c059be3d7653164e1e5e4)  chore: add "use strict" where missing
# 📦 1.0.1 (12 Feb 2022)
- [1c03a](https://gitlab.com/fatmatto/jetpack/commit/1c03ac35edc0e3254d5de51a88ce272eee78a8eb)  📦 Release 1.0.12 [skip ci]
- [1835e](https://gitlab.com/fatmatto/jetpack/commit/1835ef20579c969dd36d7351c3adc9cda584d1d1)  doc: add resume/pause docs
- [376f7](https://gitlab.com/fatmatto/jetpack/commit/376f78a5ab03db75c896fc2127a05bcd2ac883c6)  feat: add pause/resume methods to queue clients
- [2df4b](https://gitlab.com/fatmatto/jetpack/commit/2df4bd483b1eedda672fe84e208918b1e5fc3b62)  📦 Release 1.0.11 [skip ci]
- [5a750](https://gitlab.com/fatmatto/jetpack/commit/5a7501edf30efdf8dda5fb781ed054abe03ff704)  fix: emit correct event in rabbitmq error and default to empty array in franz message evt
- [15e3b](https://gitlab.com/fatmatto/jetpack/commit/15e3b3d466139cbec98169545f3114e8bef099cc)  📦 Release 1.0.10 [skip ci]
- [fa123](https://gitlab.com/fatmatto/jetpack/commit/fa1235a6bf6da081c4cc698ffc2cf5e84e2ac69f)  fix: rabbitmq publisher should publish to exchanges
# 📦 1.0.9 (21 Jan 2022)
- [afe5d](https://gitlab.com/fatmatto/jetpack/commit/afe5ddf063ac002b87f5614b7cff88b5a033000b)  📦 Release 1.0.9 [skip ci]
- [db687](https://gitlab.com/fatmatto/jetpack/commit/db6877cb9c112f4a4dba00158f28eb9d9ba98d93)  feat: add queue type rabbitmq
- [49152](https://gitlab.com/fatmatto/jetpack/commit/49152feb55f231b12c0fcaf51dfeddf94ec5645e)  chore: fix changelog generation
# 📦 1.0.8 (21 Jan 2022)
- [7654b](https://gitlab.com/fatmatto/jetpack/commit/7654bf5812b6c0a7b4105d54e07a0a2aef2c110f)  📦 Release 1.0.8 [skip ci]
- [0d466](https://gitlab.com/fatmatto/jetpack/commit/0d46652dd9885b0f95dd48961348cc31f3e28a37)  feat: add queue type rabbitmq
- [37b1c](https://gitlab.com/fatmatto/jetpack/commit/37b1cb6b02994c74bef1bc87f60783f0c4213c1f)  doc: add readme
# 📦 1.0.7 (13 Jan 2022)
- [9dac7](https://gitlab.com/fatmatto/jetpack/commit/9dac72a403c5bf9383358c7f2d28ee04da057f3a)  📦 Release 1.0.7 [skip ci]
- [6dbd4](https://gitlab.com/fatmatto/jetpack/commit/6dbd409957944bd5efc7fbc1118809f7c5b4d7e4)  fix: allow to set maxFiles and maxsize to file logger
# 📦 1.0.6 (22 Sep 2021)
- [65e3d](https://gitlab.com/fatmatto/jetpack/commit/65e3d67024ea9f264e1c1b8568a780c270a6be67)  📦 Release 1.0.6 [skip ci]
- [6653a](https://gitlab.com/fatmatto/jetpack/commit/6653aac71a701b1db2d4f5af120a60ac58b771c7)  fix: emit queue errors instead of throwing
- [f35ce](https://gitlab.com/fatmatto/jetpack/commit/f35ce442fe0e373bcf09f0499e9d6a821b3143fb)  feat: add x-request-id response header from traceid
# 📦 1.0.5 (17 Aug 2021)
- [34296](https://gitlab.com/fatmatto/jetpack/commit/3429689aca28aa8de7a92f5e5a32eacf05f4f117)  📦 Release 1.0.5 [skip ci]
- [4841e](https://gitlab.com/fatmatto/jetpack/commit/4841e15df2ed0a9309e2d6dca40e720649ae0290)  feat: change kafka library
# 📦 1.0.4 (11 Aug 2021)
- [84bb4](https://gitlab.com/fatmatto/jetpack/commit/84bb458dfb0579f1fc55ea225ef9f4e079fb8838)  📦 Release 1.0.4 [skip ci]
- [05193](https://gitlab.com/fatmatto/jetpack/commit/05193f12a022d5efea467e514bf2d961b77673a9)  fix: add tracing methods to fetch context and trace api
# 📦 1.0.3 (10 Aug 2021)
- [9b02a](https://gitlab.com/fatmatto/jetpack/commit/9b02a11a8722d9e4a0f3027d3713a5f485ab8be7)  📦 Release 1.0.3 [skip ci]
- [6ef60](https://gitlab.com/fatmatto/jetpack/commit/6ef60b0568b9e387183950ace70d35ed3312420a)  feat: add getTracer method
- [b90d8](https://gitlab.com/fatmatto/jetpack/commit/b90d873e2cb59e285c885a49d5ed0d583dbcf87a)  First Commit
